package com.apps.scit.e_store.Model;


import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class Category {
    @SerializedName("id") private int id = 0;
    @SerializedName("name") private String name = "";
    @SerializedName("full_image_url") private String full_image_url = "";
    @SerializedName("full_icon_image_url") private String full_icon_image_url = "";
    //@SerializedName("parent_id") private int parent_id = 0;
    @SerializedName("sub_categories") private List<Category> sub_categories = new ArrayList<>();

    public Category(){}

    public Category(String name) {
        this.name = name;
    }

    public Category(int id, String name, String full_image_url, String icon_image, int parent_id) {
        this.id = id;
        this.name = name;
        this.full_image_url = full_image_url;
        this.full_icon_image_url = icon_image;
        //this.parent_id = parent_id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getIcon_image() {
        return full_icon_image_url;
    }

    public void setIcon_image(String icon_image) {
        this.full_icon_image_url = icon_image;
    }

    /*

    public int getParent_id() {
        return parent_id;
    }

    public void setParent_id(int parent_id) {
        this.parent_id = parent_id;
    }

    **/

    public List<Category> getSub_categories() {
        return sub_categories;
    }

    public void setSub_categories(List<Category> sub_categories) {
        this.sub_categories = sub_categories;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return full_image_url;
    }

    public void setImage(String image) {
        this.full_image_url = image;
    }
}
