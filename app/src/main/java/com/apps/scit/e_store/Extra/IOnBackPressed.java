package com.apps.scit.e_store.Extra;

/**
 * Created by Misheal on 7/26/2018.
 */

public interface IOnBackPressed {
    boolean onBackPressed();
}
