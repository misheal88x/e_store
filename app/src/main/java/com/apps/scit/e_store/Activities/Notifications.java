package com.apps.scit.e_store.Activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.apps.scit.e_store.API.GetNotifications;
import com.apps.scit.e_store.Adapters.NotificationsAdapter;
import com.apps.scit.e_store.Adapters.OurProductsAdapter;
import com.apps.scit.e_store.Database;
import com.apps.scit.e_store.Extra.EndlessRecyclerViewScrollListener;
import com.apps.scit.e_store.Model.BaseResponse;
import com.apps.scit.e_store.Model.NotificationObject;
import com.apps.scit.e_store.Model.NotificationsResponse;
import com.apps.scit.e_store.R;
import com.google.gson.Gson;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class Notifications extends AppCompatActivity {
    Toolbar toolbar;
    TextView toolbarTitle;
    Database database;
    private ProgressBar loading;
    private int currentPage = 1;
    private int per_page = 20;
    private static String BASE_URL =  "";
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
    RecyclerView notificationsRecycler;
    List<NotificationObject> listOfNotifications;
    NotificationsAdapter notificationsAdapter;
    Intent intent;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notifications);
        toolbar = findViewById(R.id.notifications_toolbar);
        toolbarTitle = toolbar.findViewById(R.id.basicToolbarTitle);
        loading = findViewById(R.id.notifications_load);
        loading.setVisibility(View.VISIBLE);
        BASE_URL =  getResources().getString(R.string.base_url);
        database = new Database(this);
        setSupportActionBar(toolbar);
        if (getSupportActionBar()!=null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        getSupportActionBar().setTitle("");
        toolbarTitle.setText("الإشعارات");
        notificationsRecycler = findViewById(R.id.notifications_listview);
        listOfNotifications = new ArrayList<>();
        notificationsAdapter = new NotificationsAdapter(this,listOfNotifications);
        LinearLayoutManager layoutManager =new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false);
        notificationsRecycler.setLayoutManager(layoutManager);
        notificationsRecycler.setAdapter(notificationsAdapter);
        CallAPI(currentPage);
        notificationsRecycler.addOnScrollListener(new EndlessRecyclerViewScrollListener(layoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                if (listOfNotifications.size()>=per_page){
                    currentPage++;
                    CallAPI(currentPage);
                }
            }
        });
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.notifications_menu,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId()==android.R.id.home){
            this.finish();
        }
        return true;
    }

    public void CallAPI(int page){
        SharedPreferences sharedPreferences = getSharedPreferences("Profile",Context.MODE_PRIVATE);
        final String token = sharedPreferences.getString("token","");
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request newRequest  = chain.request().newBuilder()
                        .addHeader("Authorization", "Bearer " + token)
                        .build();
                return chain.proceed(newRequest);
            }
        }).build();
        Retrofit retrofit = new Retrofit.Builder().client(client).baseUrl(BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
        GetNotifications api = retrofit.create(GetNotifications.class);
        Call<BaseResponse> call;
        call = api.getNotifications(page);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                if (response.body().getCode()==200&&response.body().getMessage_code()==108){
                    String json = new Gson().toJson(response.body().getData());
                    NotificationsResponse success = new Gson().fromJson(json,NotificationsResponse.class);
                    per_page = success.getPer_page();
                    if (success.getData().size()>0){
                        loading.setVisibility(View.GONE);
                        for (NotificationObject p : success.getData()){
                            listOfNotifications.add(p);
                            notificationsAdapter.notifyDataSetChanged();
                            //listOfProducts.add(p);
                            //adapter.notifyDataSetChanged();
                        }
                        runAnimation(notificationsRecycler,0,notificationsAdapter);
                        // showActivityElements();
                    }else {
                        Toast.makeText(Notifications.this, "لا يوجد إشعارات", Toast.LENGTH_SHORT).show();
                        loading.setVisibility(View.GONE);
                    }
                }else {
                    Toast.makeText(Notifications.this, "حدث خطأ ما!!", Toast.LENGTH_SHORT).show();
                    loading.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                Toast.makeText(Notifications.this, "لا يوجد اتصال بالانترنت!", Toast.LENGTH_SHORT).show();
                loading.setVisibility(View.GONE);
            }
        });

    }

    public void runAnimation(RecyclerView recyclerView,int type,NotificationsAdapter adapter){
        Context context = recyclerView.getContext();
        LayoutAnimationController controller = null;
        if (type == 0){ // Fall down animation
            controller = AnimationUtils.loadLayoutAnimation(context,R.anim.layout_fall_down);
            recyclerView.setAdapter(adapter);
            recyclerView.setLayoutAnimation(controller);
            recyclerView.getAdapter().notifyDataSetChanged();
            recyclerView.scheduleLayoutAnimation();
        }
    }
}
