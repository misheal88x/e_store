package com.apps.scit.e_store.Fragments;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.apps.scit.e_store.API.GetAllProductsAPI;
import com.apps.scit.e_store.Activities.AllOffers;
import com.apps.scit.e_store.Activities.AllProducts;
import com.apps.scit.e_store.Adapters.MainSingleProductAdapter;
import com.apps.scit.e_store.Adapters.MostSearchedCategoriesAdapter;
import com.apps.scit.e_store.Adapters.OffersAdapter;
import com.apps.scit.e_store.Adapters.OurProductsAdapter;
import com.apps.scit.e_store.Adapters.RelatedProductsAdapter;
import com.apps.scit.e_store.Database;
import com.apps.scit.e_store.Model.BaseResponse;
import com.apps.scit.e_store.Model.Category;
import com.apps.scit.e_store.Model.Offer;
import com.apps.scit.e_store.Model.Product;
import com.apps.scit.e_store.Model.ProductsBaseObject;
import com.apps.scit.e_store.Model.SliderObject;
import com.apps.scit.e_store.R;
import com.google.gson.Gson;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class Home extends Fragment implements com.glide.slider.library.Tricks.ViewPagerEx.OnPageChangeListener {
    RecyclerView single_product_offers,most_searched_categories,most_rated_products,most_searched_products,most_visited_products,our_products;
    ViewPager offersViewpager;
    OffersAdapter offersAdapter;
    MainSingleProductAdapter mainSingleProductAdapter;
    MostSearchedCategoriesAdapter mostSearchedCategoriesAdapter;
    RelatedProductsAdapter mostRatedProductsAdapter,mostSearchedProductsAdapter,mostVisitedProductsAdapter;
    OurProductsAdapter ourProductsAdapter;
    List<Offer> listOfOffers;
    List<Product> listOfOfSingleProductOffers;
    List<Category> listOfMostSearchedCategories;
    List<Product> listOfMostRatedProducts,listOfMostSearchedProducts,listOfMostVisitedProducts,listOfOurProducts;
    com.glide.slider.library.SliderLayout slider;
    ProgressBar loadMore;
    NestedScrollView nestedScrollView;
    TextView showAllOffers,showAllSingleProductOffers,showAllMostRatedProducts,showAllMostSearchedProducts,showAllMostVisitedProducts;
    TextView mostSearchedCategoriesTxt,specialOffersTxt,normalOffersTxt,mostRatedProductsTxt,mostSearchedProductsTxt,mostVisitedProductsTxt,ourProductsTxt;
    Database database;
    int backButtonCount;
    private static final String TAG = "MainActivity";
    private int pageNumber = 1;
    private int perp_page = 10;
    private int itemCount = 10;
    //Pagination Variables
    private Boolean isLoading = true;
    private int pastVisibleItems,visibleItemCount,totalItemCount,previousTotal = 0;
    private int viewThreshold = 10;
    private static String BASE_URL;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.home_fragment,container,false);
        definingIDs(view);
        initializingSlider();
        initLists();
        gettingDataFromSQLite();
        //Initialize Adapters-------------------------------------------------------------------------
        //Offers
        offersAdapter = new OffersAdapter(getContext(),listOfOffers);
        //Single Product Offers
        mainSingleProductAdapter = new MainSingleProductAdapter(getContext(),listOfOfSingleProductOffers);
        //Most Searched Categories
        mostSearchedCategoriesAdapter = new MostSearchedCategoriesAdapter(getContext(),listOfMostSearchedCategories);
        //Most Rated Products
        mostRatedProductsAdapter = new RelatedProductsAdapter(getContext(),listOfMostRatedProducts);
        //Most Searched Products
        mostSearchedProductsAdapter = new RelatedProductsAdapter(getContext(),listOfMostSearchedProducts);
        //Most Visited Products
        mostVisitedProductsAdapter = new RelatedProductsAdapter(getContext(),listOfMostVisitedProducts);
        //Our Products
        ourProductsAdapter = new OurProductsAdapter(getContext(),listOfOurProducts);
        //Initialize LayoutManagers--------------------------------------------
        //Single Product Offers
        RecyclerView.LayoutManager SPOLayoutManager = new LinearLayoutManager(getContext(),LinearLayoutManager.HORIZONTAL,false);
        //Most Searched Categories
        RecyclerView.LayoutManager MSCLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.HORIZONTAL,false);
        //Most Rated Products
        RecyclerView.LayoutManager ClayoutManager = new LinearLayoutManager(getContext(),LinearLayoutManager.HORIZONTAL,false);
        //Most Searched Products
        RecyclerView.LayoutManager MSPLayoutManager = new LinearLayoutManager(getContext(),LinearLayoutManager.HORIZONTAL,false);
        //Most Visited Products
        RecyclerView.LayoutManager MVPLayoutManager = new LinearLayoutManager(getContext(),LinearLayoutManager.HORIZONTAL,false);
        //Our Products
        /*
        RecyclerView.LayoutManager OPlayoutManager = null;
        if(getActivity().getResources().getConfiguration().orientation==ORIENTATION_PORTRAIT){
            OPlayoutManager = new GridLayoutManager(getContext(),2);

        }else if (getActivity().getResources().getConfiguration().orientation==ORIENTATION_LANDSCAPE){
            OPlayoutManager = new GridLayoutManager(getContext(),3);
        }

**/
        final GridLayoutManager OPlayoutManager = new GridLayoutManager(getContext(),2);
        //Set LayoutManagers and Adapters to RecyclerViews----------------------
        //Offers
        offersViewpager.setAdapter(offersAdapter);
        //Single Product Offers
        single_product_offers.setLayoutManager(SPOLayoutManager);
        single_product_offers.setHasFixedSize(true);
        single_product_offers.setAdapter(mainSingleProductAdapter);
        //Most Searched Categories
        most_searched_categories.setLayoutManager(MSCLayoutManager);
        runAnimationCategories(most_searched_categories,0,mostSearchedCategoriesAdapter);
        //most_searched_categories.setAdapter(mostSearchedCategoriesAdapter);
        //Most Rated Products
        most_rated_products.setLayoutManager(ClayoutManager);
        most_rated_products.setHasFixedSize(false);
        runAnimationRelated(most_rated_products,0,mostRatedProductsAdapter);
        //most_rated_products.setAdapter(mostRatedProductsAdapter);
        //Most Searched Products
        most_searched_products.setLayoutManager(MSPLayoutManager);
        most_searched_products.setHasFixedSize(true);
        runAnimationRelated(most_searched_products,0,mostSearchedProductsAdapter);
        //most_searched_products.setAdapter(mostSearchedProductsAdapter);
        //Most Visited Products
        most_visited_products.setLayoutManager(MVPLayoutManager);
        most_visited_products.setHasFixedSize(true);
        runAnimationRelated(most_visited_products,0,mostVisitedProductsAdapter);
        //most_visited_products.setAdapter(mostVisitedProductsAdapter);
        //Our Products
        our_products.setLayoutManager(OPlayoutManager);
        our_products.setAdapter(ourProductsAdapter);
        nestedScrollView.setOnScrollChangeListener(new NestedScrollView.OnScrollChangeListener() {
            @Override
            public void onScrollChange(NestedScrollView v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                if(v.getChildAt(v.getChildCount() - 1) != null) {
                    if ((scrollY >= (v.getChildAt(v.getChildCount() - 1).getMeasuredHeight() - v.getMeasuredHeight())) &&
                            scrollY > oldScrollY) {
                        if (listOfOurProducts.size()>=perp_page){
                            pageNumber++;
                            performPagination(pageNumber);
                        }
                    }
                }
            }
        });
        //Show All Click Listeners------------------------------------------------
        //Show All Offers
        showAllOffers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(), AllOffers.class);
                startActivity(intent);
            }
        });
        showAllSingleProductOffers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(), AllOffers.class);
                startActivity(intent);
            }
        });
        //Show All Most Rated Produts
        showAllMostRatedProducts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(), AllProducts.class);
                startActivity(intent);
            }
        });
        //Show All Most Searched Produts
        showAllMostSearchedProducts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(), AllProducts.class);
                startActivity(intent);
            }
        });
        //Show All Most Visited Products
        showAllMostVisitedProducts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(), AllProducts.class);
                startActivity(intent);
            }
        });
        return view;

    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    @Override
    public void onStop() {
        //slider.stopAutoCycle();
        super.onStop();
    }
    public static float dpToPixels(int dp, Context context) {
        return dp * (context.getResources().getDisplayMetrics().density);
    }

/*
    @Override
    public boolean onBackPressed() {
        Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        return true;
    }
**/

    public class GridSpacingItemDecoration extends RecyclerView.ItemDecoration{
        private int spancount ;
        private int spacing;
        private boolean includeEdge;

        public GridSpacingItemDecoration(int spancount, int spacing, boolean includeEdge) {
            this.spancount = spancount;
            this.spacing = spacing;
            this.includeEdge = includeEdge;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            int position = parent.getChildAdapterPosition(view); //item position
            int column = position % spancount; //item column
            if (includeEdge){
                outRect.left = spacing - column * spacing / spancount;
                outRect.right = (column + 1) * spacing / spancount;
                if (position < spancount){ //top edge
                    outRect.top = spacing;
                }
                outRect.bottom = spacing; //item bottom
            }else{
                outRect.left = column *spacing / spancount;
                outRect.right = spacing - (column + 1) * spacing / spancount;
                if (position >= spancount){
                    outRect.top = spacing; //item top
                }
            }
        }
    }
    private int dpToPx(int dp){
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,dp,r.getDisplayMetrics()));
    }
    /*
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

        // Checks the orientation of the screen
        if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
            RecyclerView.LayoutManager OPlayoutManager = new GridLayoutManager(getContext(),3);
            our_products.setLayoutManager(OPlayoutManager);
        } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT){
            RecyclerView.LayoutManager OPlayoutManager = new GridLayoutManager(getContext(),2);
            our_products.setLayoutManager(OPlayoutManager);
        }
    }
    **/
    public void definingIDs(View view){
        //Base url
        BASE_URL = getResources().getString(R.string.base_url);
        // Database
        database = new Database(getContext());
        // Slider
        slider = view.findViewById(R.id.slider);
        // View Pager
        offersViewpager = view.findViewById(R.id.offers_viewpager);
        // RecyclerViews
        most_searched_categories = view.findViewById(R.id.most_searched_categories);
        single_product_offers = view.findViewById(R.id.main_single_product_offer_recycler);
        most_rated_products = view.findViewById(R.id.most_rated_products_recycler);
        most_searched_products = view.findViewById(R.id.most_searched_products_recycler);
        most_visited_products = view.findViewById(R.id.most_visited_products_recycler);
        our_products = view.findViewById(R.id.our_products_recycler);
        // TextViews
        showAllOffers = view.findViewById(R.id.view_all_offers);
        showAllSingleProductOffers = view.findViewById(R.id.view_all_single_product_offers);
        showAllMostRatedProducts = view.findViewById(R.id.view_all_most_rated_products);
        showAllMostSearchedProducts = view.findViewById(R.id.view_all_most_searched_products);
        showAllMostVisitedProducts = view.findViewById(R.id.view_all_most_visited_products);

        mostSearchedCategoriesTxt = view.findViewById(R.id.most_searched_categories_text);
        specialOffersTxt = view.findViewById(R.id.offers_text);
        normalOffersTxt = view.findViewById(R.id.main_single_product_offer_text);
        mostRatedProductsTxt = view.findViewById(R.id.most_rated_products_text);
        mostSearchedProductsTxt = view.findViewById(R.id.most_searched_products_text);
        mostVisitedProductsTxt = view.findViewById(R.id.most_visited_products_text);
        ourProductsTxt = view.findViewById(R.id.our_products_text);
        //ProgressBar
        loadMore = view.findViewById(R.id.home_load_more);
        loadMore.setVisibility(View.GONE);
        //Nested ScrollView
        nestedScrollView = view.findViewById(R.id.home_nested_scrollview);
    }
    public void initializingSlider(){
        ArrayList<String> listUrl = new ArrayList<>();
        ArrayList<String> listName = new ArrayList<>();
        if (database.getSliders().size()>0){
            for (SliderObject s : database.getSliders()){
                listUrl.add(s.getImage());
                listName.add(s.getTitle());
            }
            RequestOptions requestOptions = new RequestOptions();
            requestOptions
                    .centerCrop().diskCacheStrategy(DiskCacheStrategy.ALL)
                    .error(R.drawable.offlinecropped).placeholder(R.drawable.offlinecropped);
            for (int i = 0; i < listUrl.size(); i++) {
                com.glide.slider.library.SliderTypes.DefaultSliderView sliderView = new com.glide.slider.library.SliderTypes.DefaultSliderView(getContext());
                sliderView
                        .image(listUrl.get(i))
                        .setBackgroundColor(getResources().getColor(R.color.lightGrey))
                        .setRequestOption(requestOptions)
                        .setProgressBarVisible(true);
                sliderView.bundle(new Bundle());
                sliderView.getBundle().putString("extra", listName.get(i));
                slider.addSlider(sliderView);
            }
            slider.setPresetTransformer(com.glide.slider.library.SliderLayout.Transformer.Default);
            slider.setPresetIndicator(com.glide.slider.library.SliderLayout.PresetIndicators.Center_Bottom);
            slider.setCustomAnimation(new com.glide.slider.library.Animations.DescriptionAnimation());
            slider.setDuration(4000);
            slider.addOnPageChangeListener(this);
        }
        /*
        if (database.getSliders().size()>0){
            HashMap<String,String> url_maps = new HashMap<String, String>();
            for (SliderObject s : database.getSliders()){
                url_maps.put(s.getTitle(),s.getImage());
            }
            for (String name : url_maps.keySet()){
                DefaultSliderView defaultSliderView = new DefaultSliderView(getContext());
                defaultSliderView.image(url_maps.get(name)).
                        error(R.drawable.offlinecropped)
                        .setScaleType(BaseSliderView.ScaleType.CenterCrop).empty(R.drawable.offlinecropped);

                defaultSliderView.bundle(new Bundle());
                defaultSliderView.getBundle()
                        .putString("extra",name);
                slider.addSlider(defaultSliderView);
            }
            slider.setPresetTransformer(SliderLayout.Transformer.Accordion);
            slider.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
            slider.setCustomAnimation(new DescriptionAnimation());
            slider.setDuration(4000);
            slider.addOnPageChangeListener(this);
            slider.setPresetTransformer("Default");
        }else {
            slider.setVisibility(View.GONE);
        }
        **/
    }
    private void initLists(){
        listOfOffers = new ArrayList<>();
        listOfOfSingleProductOffers = new ArrayList<>();
        listOfMostSearchedCategories = new ArrayList<>();
        listOfMostRatedProducts = new ArrayList<>();
        listOfMostRatedProducts = new ArrayList<>();
        listOfMostSearchedProducts = new ArrayList<>();
        listOfMostVisitedProducts = new ArrayList<>();
        listOfOurProducts = new ArrayList<>();
    }
    public void gettingDataFromSQLite(){
        //Special Offers
        if (database.getSpecialOffers().size()>0){

            for (Offer offer : database.getSpecialOffers()){
                listOfOffers.add(offer);
            }
        }else {

            showAllOffers.setVisibility(View.GONE);
            specialOffersTxt.setVisibility(View.GONE);
            offersViewpager.setVisibility(View.GONE);
        }

        //Normal Offers
        if (database.getNormalOffers().size()>0){

            for (Product p : database.getNormalOffers()){
                listOfOfSingleProductOffers.add(p);
            }
        }else {

            showAllSingleProductOffers.setVisibility(View.GONE);
            normalOffersTxt.setVisibility(View.GONE);
            single_product_offers.setVisibility(View.GONE);
        }

        //Categories
        if (database.getCategories(0).size()>0){

            int index = 1;
            for (Category c : database.getCategories(0)){
                listOfMostSearchedCategories.add(c);
                if (index==4||index==database.getCategories(0).size()){
                    listOfMostSearchedCategories.add(new Category("الكل"));
                    break;
                }
                    index++;
            }
        }else {

            mostSearchedCategoriesTxt.setVisibility(View.GONE);
            most_searched_categories.setVisibility(View.GONE);
        }

        //Most Rated Products
        if (database.getTopRatedProducts().size()>0){

            for (Product p : database.getTopRatedProducts()){
                listOfMostRatedProducts.add(p);
            }
        }else {

            mostRatedProductsTxt.setVisibility(View.GONE);
            showAllMostRatedProducts.setVisibility(View.GONE);
            most_rated_products.setVisibility(View.GONE);
        }

        //Most Searched Products

        if (database.getTopSearchedProducts().size()>0){
            for (Product p : database.getTopSearchedProducts()){
                listOfMostSearchedProducts.add(p);
            }
        }else {
            mostSearchedProductsTxt.setVisibility(View.GONE);
            showAllMostSearchedProducts.setVisibility(View.GONE);
            most_searched_products.setVisibility(View.GONE);
        }

        //Most Visited Products

        if (database.getTopViewedProducts().size()>0){
            for (Product p : database.getTopViewedProducts()){
                listOfMostVisitedProducts.add(p);
            }
        }else {
            mostVisitedProductsTxt.setVisibility(View.GONE);
            showAllMostVisitedProducts.setVisibility(View.GONE);
            most_visited_products.setVisibility(View.GONE);
        }

        //Our Products

        if (database.getProducts().size()>0){
            for (Product p : database.getProducts()){
                if (listOfOurProducts.size()<10){
                    listOfOurProducts.add(p);
                }
            }
        }else {
            ourProductsTxt.setVisibility(View.GONE);
            our_products.setVisibility(View.GONE);
        }
    }
    public void performPagination(int page){
        loadMore.setVisibility(View.VISIBLE);
        SharedPreferences sharedPreferences = getContext().getSharedPreferences("Profile",Context.MODE_PRIVATE);
        final String token = sharedPreferences.getString("token","");
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request newRequest  = chain.request().newBuilder()
                        .addHeader("Authorization", "Bearer " + token)
                        .build();
                return chain.proceed(newRequest);
            }
        }).build();
        Retrofit retrofit = new Retrofit.Builder().client(client).baseUrl(BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
        GetAllProductsAPI getAllProductsAPI = retrofit.create(GetAllProductsAPI.class);
        Call<BaseResponse> call;
        call = getAllProductsAPI.getProducts(page);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                if (response.body().getCode()==200&&response.body().getMessage_code()==108){
                    String json = new Gson().toJson(response.body().getData());
                    ProductsBaseObject success = new Gson().fromJson(json,ProductsBaseObject.class);
                    perp_page = success.getPer_page();
                    if (success.getData().size()>0){
                        for (Product p : success.getData()){
                            listOfOurProducts.add(p);
                            ourProductsAdapter.notifyDataSetChanged();
                        }

                    }else {
                        Toast.makeText(getContext(), "لا يوجد منتجات إضافية", Toast.LENGTH_SHORT).show();
                    }
                }else {
                    Toast.makeText(getContext(), "حدث خطأ ما!!", Toast.LENGTH_SHORT).show();
                }
                loadMore.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {

            }
        });
    }
    public void runAnimationCategories(RecyclerView recyclerView,int type,MostSearchedCategoriesAdapter adapter){
        Context context = recyclerView.getContext();
        LayoutAnimationController controller = null;
        if (type == 0){ // Fall down animation
            controller = AnimationUtils.loadLayoutAnimation(context,R.anim.layout_fall_down);
            recyclerView.setAdapter(adapter);
            recyclerView.setLayoutAnimation(controller);
            recyclerView.getAdapter().notifyDataSetChanged();
            recyclerView.scheduleLayoutAnimation();
        }
    }
    public void runAnimationRelated(RecyclerView recyclerView,int type,RelatedProductsAdapter adapter){
        Context context = recyclerView.getContext();
        LayoutAnimationController controller = null;
        if (type == 0){ // Fall down animation
            controller = AnimationUtils.loadLayoutAnimation(context,R.anim.layout_fall_down);
            recyclerView.setAdapter(adapter);
            recyclerView.setLayoutAnimation(controller);
            recyclerView.getAdapter().notifyDataSetChanged();
            recyclerView.scheduleLayoutAnimation();
        }
    }
}
