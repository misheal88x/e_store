package com.apps.scit.e_store.API;

import com.apps.scit.e_store.Model.BaseResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by Misheal on 7/30/2018.
 */

public interface SearchAPI {
    @FormUrlEncoded
    @POST("api/v1/search")
    Call<BaseResponse> search(@Query("page")int page,
                              @Field("keywords")String keywords,
                              @Field("order_flag")String order_flag,
                              @Field("categories_flag")int categories_flag,
                              @Field("products_flag")int products_flag,
                              @Field("offers_flag")int offers_flag);
}
