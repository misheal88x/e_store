package com.apps.scit.e_store.Adapters;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.CountDownTimer;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.apps.scit.e_store.Activities.OfferDetails;
import com.apps.scit.e_store.Database;
import com.apps.scit.e_store.Model.Offer;
import com.apps.scit.e_store.R;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.glide.slider.library.svg.GlideApp;

import java.util.List;


public class OffersAdapter extends PagerAdapter{
    private Context context;
    private List<Offer> listOfOffers;
    private LayoutInflater layoutInflater;
    private CountDownTimer timer;
    Database database;
    public OffersAdapter(Context context,List<Offer> list){
        this.context = context;
        this.listOfOffers = list;
        this.database = new Database(context);
    }
    @Override
    public int getCount() {
        return listOfOffers.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(final ViewGroup container, int position) {
        layoutInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final Offer offer = listOfOffers.get(position);
        View view = layoutInflater.inflate(R.layout.offer_card,null);
        ImageView offer_image= view.findViewById(R.id.offer_image);
        final TextView remaining_time = view.findViewById(R.id.offer_expired_time);
        CardView cardView = view.findViewById(R.id.offer_card);
        if (offer.getImage_url()!=null){
            GlideApp.with(context).load(offer.getImage_url())
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .placeholder(R.drawable.offline).centerCrop().into(offer_image);
            //Picasso.with(context).load(offer.getImage_url()).placeholder(R.drawable.offline).into(offer_image);
        }
        if (offer.getOffer_remain().equals("")){
            remaining_time.setText("00:00:00");
        }else {
            long remainingTime = Long.parseLong(offer.getOffer_remain().substring(0,offer.getOffer_remain().length()-2));

            timer = new CountDownTimer(remainingTime*1000,500) {
                @Override
                public void onTick(long l) {
                    remaining_time.setText(formatSeconds(l/1000));
                    database.updateSpecialOfferRemain(offer.getId(),l/1000);
                }

                @Override
                public void onFinish() {

                }
            }.start();

        }

        //remaining_time.setText(offer.getEnd_date());
        cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences sharedPreferences = context.getSharedPreferences("SpecialOffer",Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putInt("offer_id",offer.getId());
                editor.commit();
                Intent intent = new Intent(context,OfferDetails.class);
                intent.putExtra("offer_id",String.valueOf(offer.getId()));
                context.startActivity(intent);
            }
        });
        ViewPager viewPager = (ViewPager)container;
        viewPager.addView(view,0);
        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        ViewPager viewPager = (ViewPager)container;
        View view = (View)object;
        viewPager.removeView(view);
    }
    public  String formatSeconds(long totalSecs){
        long days = (int)(totalSecs/86400);
        long hours =  (int)((totalSecs % 86400) / 3600);
        long minutes =  (int)((totalSecs % 3600) / 60);
        long seconds =  (int)(totalSecs % 60);
        if (days==0){
            return String.format("%02d:%02d:%02d", hours, minutes, seconds);
        }else {
            return String.format("%02d:%02d:%02d", hours, minutes, seconds)+" و "+String.valueOf(days)+" يوم";
        }

    }
}
