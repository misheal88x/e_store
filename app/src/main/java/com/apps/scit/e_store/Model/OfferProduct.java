package com.apps.scit.e_store.Model;

/**
 * Created by Misheal on 6/26/2018.
 */

public class OfferProduct {
    private String productname;
    private String productCategory;
    private float productRate;
    private String oldPrice;
    private String newPrice;
    private Integer productImage;

    public OfferProduct(String productname, float productRate, String oldPrice, String newPrice, Integer productImage) {
        this.productname = productname;
        this.productRate = productRate;
        this.oldPrice = oldPrice;
        this.newPrice = newPrice;
        this.productImage = productImage;
    }
    public OfferProduct(String productname, float productRate, String oldPrice, String newPrice) {
        this.productname = productname;
        this.productRate = productRate;
        this.oldPrice = oldPrice;
        this.newPrice = newPrice;
    }
    public OfferProduct(String productname,String productCategory, float productRate, String oldPrice, String newPrice) {
        this.productname = productname;
        this.productCategory = productCategory;
        this.productRate = productRate;
        this.oldPrice = oldPrice;
        this.newPrice = newPrice;
    }

    public String getProductname() {
        return productname;
    }

    public void setProductname(String productname) {
        this.productname = productname;
    }

    public String getProductCategory() {
        return productCategory;
    }

    public void setProductCategory(String productCategory) {
        this.productCategory = productCategory;
    }

    public float getProductRate() {
        return productRate;
    }

    public void setProductRate(float productRate) {
        this.productRate = productRate;
    }

    public String getOldPrice() {
        return oldPrice;
    }

    public void setOldPrice(String oldPrice) {
        this.oldPrice = oldPrice;
    }

    public String getNewPrice() {
        return newPrice;
    }

    public void setNewPrice(String newPrice) {
        this.newPrice = newPrice;
    }

    public Integer getProductImage() {
        return productImage;
    }

    public void setProductImage(Integer productImage) {
        this.productImage = productImage;
    }
}
