package com.apps.scit.e_store.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.apps.scit.e_store.Adapters.OfferProductsAdapter;
import com.apps.scit.e_store.Adapters.OffersAdapter;
import com.apps.scit.e_store.Database;
import com.apps.scit.e_store.Model.Offer;
import com.apps.scit.e_store.Model.Product;
import com.apps.scit.e_store.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Misheal on 6/19/2018.
 */

public class Offers extends Fragment {
    RecyclerView singleProductOffersRecycler;
    ViewPager offersViewPager;
    List<Offer> listOfOffers;
    List<Product> listOfSingleProductOffers;
    OffersAdapter offersAdapter;
    OfferProductsAdapter singleProductOffersAdapter;
    NestedScrollView scrollView;
    TextView specialOffersText,normalOffersText;
    LinearLayout NormalOffersLinearLayout;
    Database database;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.offers_fragment,container,false);
        definingIDs(view);
        //Setting up ScrollView
        scrollView.setFocusableInTouchMode(true);
        scrollView.setDescendantFocusability(ViewGroup.FOCUS_BEFORE_DESCENDANTS);
        getDataFromSQLite();
        RecyclerView.LayoutManager layoutManager2 = new GridLayoutManager(getContext(),2);
        singleProductOffersRecycler.setLayoutManager(layoutManager2);
        offersAdapter = new OffersAdapter(getContext(),listOfOffers);
        singleProductOffersAdapter = new OfferProductsAdapter(getContext(),listOfSingleProductOffers);
        offersViewPager.setAdapter(offersAdapter);
        runAnimation(singleProductOffersRecycler,0,singleProductOffersAdapter);
        //singleProductOffersRecycler.setAdapter(singleProductOffersAdapter);
        return view;
    }
    public void definingIDs(View view){
        //ScrollView
        scrollView = view.findViewById(R.id.offer_scrollview1);
        //TextView
        specialOffersText = view.findViewById(R.id.multiple_products_offers_text);
        normalOffersText = view.findViewById(R.id.single_product_offers_text);
        //LinearLayout
        NormalOffersLinearLayout = view.findViewById(R.id.normal_offers_linearlayout);
        //Database
        database = new Database(getContext());
        //View Pager
        offersViewPager = view.findViewById(R.id.multiple_products_offers_viewpager);
        //RecyclerView
        singleProductOffersRecycler = view.findViewById(R.id.single_product_offers_recycler);
    }
    public void getDataFromSQLite(){

        listOfOffers = new ArrayList<>();
        listOfSingleProductOffers = new ArrayList<>();
        if (database.getSpecialOffers().size()==0&&database.getNormalOffers().size()==0){
            Toast.makeText(getContext(), "لا يوجد عروض", Toast.LENGTH_SHORT).show();
            specialOffersText.setVisibility(View.GONE);
            offersViewPager.setVisibility(View.GONE);
            NormalOffersLinearLayout.setVisibility(View.GONE);
            normalOffersText.setVisibility(View.GONE);
        }
        //Special Offers
        if (database.getSpecialOffers().size()>0){
            for (Offer o : database.getSpecialOffers()){
                listOfOffers.add(o);
            }
        }else {
            specialOffersText.setVisibility(View.GONE);
            offersViewPager.setVisibility(View.GONE);
        }
        //Normal Offers
        if (database.getNormalOffers().size()>0){
            for (Product p : database.getNormalOffers()){
                listOfSingleProductOffers.add(p);
            }
        }else {
            NormalOffersLinearLayout.setVisibility(View.GONE);
            normalOffersText.setVisibility(View.GONE);
        }
    }

    /*
    @Override
    public boolean onBackPressed() {
        return true;
    }
    **/
    public void runAnimation(RecyclerView recyclerView,int type,OfferProductsAdapter adapter){
        Context context = recyclerView.getContext();
        LayoutAnimationController controller = null;
        if (type == 0){ // Fall down animation
            controller = AnimationUtils.loadLayoutAnimation(context,R.anim.layout_fall_down);
            recyclerView.setAdapter(adapter);
            recyclerView.setLayoutAnimation(controller);
            recyclerView.getAdapter().notifyDataSetChanged();
            recyclerView.scheduleLayoutAnimation();
        }
    }
}
