package com.apps.scit.e_store.Activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.TextView;
import android.widget.Toast;

import com.apps.scit.e_store.Adapters.CategoriesAdapter;
import com.apps.scit.e_store.Database;
import com.apps.scit.e_store.Model.Category;
import com.apps.scit.e_store.R;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class AllCategories extends AppCompatActivity {
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
    RecyclerView categoriesRecycler;
    List<Category> listOfCategories;
    CategoriesAdapter categoriesAdapter;
    Toolbar toolbar;
    TextView toolbarTitle;
    Database database;
    Stack<Integer> parent_id_stack;
    int baseCategoryID = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_categories);
        toolbar = findViewById(R.id.all_categories_toolbar);
        toolbarTitle = toolbar.findViewById(R.id.basicToolbarTitle);
        setSupportActionBar(toolbar);
        if (getSupportActionBar()!=null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        getSupportActionBar().setTitle("");
        toolbarTitle.setText("الأصناف");
        database = new Database(this);
        categoriesRecycler = findViewById(R.id.all_categories_recycler);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        categoriesRecycler.setLayoutManager(layoutManager);
        listOfCategories = new ArrayList<>();
        parent_id_stack = new Stack<>();
        final SharedPreferences sharedPreferences = getSharedPreferences("Category",Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        if (sharedPreferences.getBoolean("FromMain",false)){
            baseCategoryID = sharedPreferences.getInt("category_id",0);
            parent_id_stack.push(baseCategoryID);
            toolbarTitle.setText(sharedPreferences.getString("category_name",""));
            if (database.getCategories(baseCategoryID).size()>0){
                for (Category c : database.getCategories(baseCategoryID)){
                    listOfCategories.add(c);
                }
            }else {
                SharedPreferences sharedPreferences1 = getSharedPreferences("Category",Context.MODE_PRIVATE);
                SharedPreferences.Editor editor1= sharedPreferences1.edit();
                editor1.putInt("category_id",baseCategoryID);
                editor1.commit();
                Intent intent = new Intent(AllCategories.this,CategoryProducts.class);
                startActivity(intent);
            }
        }else {
            parent_id_stack.push(baseCategoryID);
            if (database.getCategories(baseCategoryID).size()>0){
                for (Category c : database.getCategories(baseCategoryID)){
                    listOfCategories.add(c);
                }
            }else {
                Toast.makeText(this, "لا يوجد أصناف!", Toast.LENGTH_SHORT).show();
                finish();
            }
        }


        categoriesAdapter = new CategoriesAdapter(this,listOfCategories);
        runAnimation(categoriesRecycler,0,categoriesAdapter);
        //categoriesRecycler.setAdapter(categoriesAdapter);
        categoriesAdapter.setOnItemClickListener(new CategoriesAdapter.OnItemClickListenter() {
            @Override
            public void onItemClick(int position) {
                if (database.getCategories(listOfCategories.get(position).getId()).size()>0){
                    parent_id_stack.push(listOfCategories.get(position).getId());
                    int id = listOfCategories.get(position).getId();
                    toolbarTitle.setText(listOfCategories.get(position).getName());
                    listOfCategories.clear();
                    for (Category c : database.getCategories(id)){
                        listOfCategories.add(c);
                        categoriesAdapter.notifyDataSetChanged();
                    }

                }else {
                    SharedPreferences sharedPreferences = getSharedPreferences("Category",Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor= sharedPreferences.edit();
                    editor.putInt("category_id",listOfCategories.get(position).getId());
                    editor.putString("category_name",listOfCategories.get(position).getName());
                    editor.commit();
                    Intent intent = new Intent(AllCategories.this,CategoryProducts.class);
                    startActivity(intent);
                }

            }
        });
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.all_categories_menu,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId()==android.R.id.home){
            SharedPreferences sharedPreferences = getSharedPreferences("Category",Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPreferences.edit();
            if (sharedPreferences.getBoolean("FromMain",false)){
                if (parent_id_stack.peek()==baseCategoryID){
                    editor.putBoolean("FromMain",false);
                    editor.commit();
                    finish();
                }else {
                    parent_id_stack.pop();
                    if (database.getCategories(parent_id_stack.peek()).size()>0){
                        if (parent_id_stack.peek()==baseCategoryID){
                            toolbarTitle.setText("الأصناف");
                        }else {
                            toolbarTitle.setText(database.getSingleCategory(parent_id_stack.peek()).getName());
                        }
                        listOfCategories.clear();
                        for (Category c : database.getCategories(parent_id_stack.peek())){
                            listOfCategories.add(c);
                            categoriesAdapter.notifyDataSetChanged();
                        }
                    }else {
                        editor.putBoolean("FromMain",false);
                        editor.commit();
                        finish();
                    }
                }
            }else {
                if (parent_id_stack.peek()==baseCategoryID){
                    finish();
                }else {
                    parent_id_stack.pop();
                    if (parent_id_stack.peek()==baseCategoryID){
                        toolbarTitle.setText("الأصناف");
                    }else {
                        toolbarTitle.setText(database.getSingleCategory(parent_id_stack.peek()).getName());
                    }
                    listOfCategories.clear();
                    for (Category c : database.getCategories(parent_id_stack.peek())){
                        listOfCategories.add(c);
                        categoriesAdapter.notifyDataSetChanged();
                    }
                }
            }

        }
        return true;
    }

    @Override
    public void onBackPressed() {
        SharedPreferences sharedPreferences = getSharedPreferences("Category",Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        if (sharedPreferences.getBoolean("FromMain",false)){
            if (parent_id_stack.peek()==baseCategoryID){
                editor.putBoolean("FromMain",false);
                editor.commit();
                finish();
            }else {
                parent_id_stack.pop();
                if (parent_id_stack.peek()==baseCategoryID){
                    toolbarTitle.setText("الأصناف");
                }else {
                    toolbarTitle.setText(database.getSingleCategory(parent_id_stack.peek()).getName());
                }
                if (database.getCategories(parent_id_stack.peek()).size()>0){
                    listOfCategories.clear();
                    for (Category c : database.getCategories(parent_id_stack.peek())){
                        listOfCategories.add(c);
                        categoriesAdapter.notifyDataSetChanged();
                    }
                }else {
                    editor.putBoolean("FromMain",false);
                    editor.commit();
                    finish();
                }
            }
        }else {
            if (parent_id_stack.peek()==baseCategoryID){
                finish();
            }else {
                parent_id_stack.pop();
                if (parent_id_stack.peek()==baseCategoryID){
                    toolbarTitle.setText("الأصناف");
                }else {
                    toolbarTitle.setText(database.getSingleCategory(parent_id_stack.peek()).getName());
                }
                listOfCategories.clear();
                for (Category c : database.getCategories(parent_id_stack.peek())){
                    listOfCategories.add(c);
                    categoriesAdapter.notifyDataSetChanged();
                }
            }
        }

    }
    public void runAnimation(RecyclerView recyclerView,int type,CategoriesAdapter adapter){
        Context context = recyclerView.getContext();
        LayoutAnimationController controller = null;
        if (type == 0){ // Fall down animation
            controller = AnimationUtils.loadLayoutAnimation(context,R.anim.layout_fall_down_vertical);
            recyclerView.setAdapter(adapter);
            recyclerView.setLayoutAnimation(controller);
            recyclerView.getAdapter().notifyDataSetChanged();
            recyclerView.scheduleLayoutAnimation();
        }
    }
}
