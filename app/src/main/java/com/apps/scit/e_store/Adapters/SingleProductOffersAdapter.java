package com.apps.scit.e_store.Adapters;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Paint;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.apps.scit.e_store.Activities.ProductDetails;
import com.apps.scit.e_store.Model.Product;
import com.apps.scit.e_store.R;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.glide.slider.library.svg.GlideApp;

import java.util.List;

/**
 * Created by Misheal on 7/1/2018.
 */

public class SingleProductOffersAdapter extends RecyclerView.Adapter<SingleProductOffersAdapter.MyViewHolder> {
    private Context context;
    private List<Product> listOfOffers;
    public class MyViewHolder extends RecyclerView.ViewHolder{
        public TextView offerName,offerOldPrice,offerNewPrice;
        public ImageView offerImage;
        public RelativeLayout layout;
        public MyViewHolder(View view){
            super(view);
            offerName = view.findViewById(R.id.single_products_offer_row_name);
            offerOldPrice = view.findViewById(R.id.single_product_offer_old_price);
            offerNewPrice = view.findViewById(R.id.single_product_offer_new_price);
            offerImage = view.findViewById(R.id.single_product_offer_row_image);
            offerOldPrice.setPaintFlags(offerOldPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
            layout = view.findViewById(R.id.single_product_offer_row_layout);
        }
    }
    public SingleProductOffersAdapter(Context context, List<Product> listOfOffers){
        this.context = context;
        this.listOfOffers = listOfOffers;
    }
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.single_product_offer_row,parent,false);
        return new MyViewHolder(itemView);
    }
    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        final Product offer = listOfOffers.get(position);
        holder.offerName.setText(offer.getName());
        holder.offerOldPrice.setText(String.valueOf(Math.round(offer.getPrice())));
        holder.offerNewPrice.setText(String.valueOf(Math.round(offer.getOffer_price())));
        if (offer.getImage()!=null){
            try {
                GlideApp.with(context).load(offer.getImage())
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .placeholder(R.drawable.offline).centerCrop().into(holder.offerImage);
                }catch (Exception e){}
        }
        holder.layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences sharedPreferences = context.getSharedPreferences("product",Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString("productType","offer");
                editor.putInt("product_id",offer.getId());
                editor.commit();
                Intent intent = new Intent(context,ProductDetails.class);
                intent.putExtra("product_id",String.valueOf(offer.getId()));
                intent.putExtra("productType","offer");
                context.startActivity(intent);
            }
        });
    }
    @Override
    public int getItemCount() {
        return listOfOffers.size();
    }
}
