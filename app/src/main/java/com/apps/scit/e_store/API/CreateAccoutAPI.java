package com.apps.scit.e_store.API;

import com.apps.scit.e_store.Model.BaseResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.POST;


/**
 * Created by Misheal on 7/8/2018.
 */

public interface CreateAccoutAPI {
    @FormUrlEncoded
    @POST("api/v1/client/create_account")
    Call<BaseResponse> create(@Header("SHOP-TOKEN") String SHOP_TOKEN,
                              @Field("device_id")String device_id,
                              @Field("signature")String signature,
                              @Field("platform")String platform);
}
