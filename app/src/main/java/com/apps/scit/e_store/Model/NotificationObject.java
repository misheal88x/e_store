package com.apps.scit.e_store.Model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Misheal on 6/23/2019.
 */

public class NotificationObject {
    @SerializedName("id") private int id = 0;
    @SerializedName("user_id") private int user_id = 0;
    @SerializedName("target") private int target = 0;
    @SerializedName("type") private int type = 0;
    @SerializedName("content") private String content = "";
    @SerializedName("created_at") private String created_at = "";

    public NotificationObject() {}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public int getTarget() {
        return target;
    }

    public void setTarget(int target) {
        this.target = target;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }
}
