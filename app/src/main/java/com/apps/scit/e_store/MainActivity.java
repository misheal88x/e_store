package com.apps.scit.e_store;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.apps.scit.e_store.API.UpdateProfileAPI;
import com.apps.scit.e_store.Activities.Notifications;
import com.apps.scit.e_store.Extra.IOnBackPressed;
import com.apps.scit.e_store.Fragments.About;
import com.apps.scit.e_store.Fragments.Categories;
import com.apps.scit.e_store.Fragments.Connect;
import com.apps.scit.e_store.Fragments.Home;
import com.apps.scit.e_store.Fragments.Offers;
import com.apps.scit.e_store.Fragments.Products;
import com.apps.scit.e_store.Fragments.Search;
import com.apps.scit.e_store.Fragments.Settings;
import com.apps.scit.e_store.Fragments.We;
import com.apps.scit.e_store.Model.BaseResponse;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    RelativeLayout toolbar;
    ImageButton search,notifications,launchDrawer;
    TextView title;
    Fragment currentFragment;
    NavigationView navigationView;
    int numBackPressed = 0;
    Database database;
    private static String BASE_URL;
    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
    //Toolbar toolbar;
    DrawerLayout drawer;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if (getWindow().getDecorView().getLayoutDirection() == View.LAYOUT_DIRECTION_LTR){
            getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
        }
        //Base url
        BASE_URL = getResources().getString(R.string.base_url);
        database = new Database(MainActivity.this);
        try {
            if(FirebaseInstanceId.getInstance().getToken()!=null) {
                String token = FirebaseInstanceId.getInstance().getToken();
                Log.e("MainActivity","the token: "+token);
                SharedPreferences sharedPreferences = getSharedPreferences("Profile",Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString("notificationsToken",token);
                editor.commit();
                callUpdateProfileAPI(token);
            }
        }catch (Exception c){}

        navigationView = findViewById(R.id.nav_view);
        toolbar = findViewById(R.id.relativeToolbar);
        search = findViewById(R.id.toolBarSearch);
        notifications = findViewById(R.id.toolBarNotifications);
        launchDrawer = findViewById(R.id.toolBarLaunchDrawer);
        title = findViewById(R.id.toolBarTitle);
        //toolbar = (Toolbar) findViewById(R.id.toolbar);
        //setSupportActionBar(toolbar);
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        //ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
        //        this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        //drawer.addDrawerListener(toggle);
        //toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        //getSupportActionBar().setTitle("الرئيسية");
        title.setText("الرئيسية");
        launchDrawer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawer.openDrawer(Gravity.RIGHT);
            }
        });
        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Intent intent = new Intent(MainActivity.this, SearchActivity.class);
                //startActivity(intent);
                title.setText("البحث");
                Search s=new Search();
                FragmentTransaction transaction=getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.fragmentcontainer,s).addToBackStack(null).commit();
                //transaction.commit();
            }
        });
        notifications.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, Notifications.class);
                startActivity(intent);
            }
        });
        firstFragmentLaunch();
    }

    @Override
    public void onBackPressed() {
        SharedPreferences sharedPreferences = getSharedPreferences("Extra",Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fragmentcontainer);
        if (!(fragment instanceof IOnBackPressed) || !((IOnBackPressed) fragment).onBackPressed()) {
            //super.onBackPressed();
        }
            if (currentFragment instanceof  Home&&sharedPreferences.getBoolean("searchFragmanet",false)){
                title.setText("الرئيسية");
                Home h=new Home();
                currentFragment = h;
                FragmentTransaction transaction=getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.fragmentcontainer,h);
                toolbar.setBackgroundColor(getResources().getColor(R.color.lightGrey));
                search.setBackgroundColor(getResources().getColor(R.color.lightGrey));
                notifications.setBackgroundColor(getResources().getColor(R.color.lightGrey));
                launchDrawer.setBackgroundColor(getResources().getColor(R.color.lightGrey));
                transaction.commit();
                navigationView.getMenu().getItem(0).setChecked(true);
                editor.putBoolean("searchFragmanet",false);
                editor.commit();
            } else if (currentFragment instanceof Home) {
            if (numBackPressed==1){
                Intent intent = new Intent(Intent.ACTION_MAIN);
                intent.addCategory(Intent.CATEGORY_HOME);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
            }else {
                Toast.makeText(this, "إضغط على زر العودة مرة أخرى للخروج من التطبيق", Toast.LENGTH_SHORT).show();
                numBackPressed++;
            }
            } else if (!(currentFragment instanceof Categories)){
                title.setText("الرئيسية");
                Home h=new Home();
                currentFragment = h;
                FragmentTransaction transaction=getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.fragmentcontainer,h);
                toolbar.setBackgroundColor(getResources().getColor(R.color.lightGrey));
                search.setBackgroundColor(getResources().getColor(R.color.lightGrey));
                notifications.setBackgroundColor(getResources().getColor(R.color.lightGrey));
                launchDrawer.setBackgroundColor(getResources().getColor(R.color.lightGrey));
                transaction.commit();
                navigationView.getMenu().getItem(0).setChecked(true);
            }else if (currentFragment instanceof Categories&&sharedPreferences.getBoolean("lastBackPage",false)){
                //currentMenuItem = R.id.nav_home;
                title.setText("الرئيسية");
                Home h=new Home();
                currentFragment = h;
                FragmentTransaction transaction=getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.fragmentcontainer,h);
                toolbar.setBackgroundColor(getResources().getColor(R.color.lightGrey));
                search.setBackgroundColor(getResources().getColor(R.color.lightGrey));
                notifications.setBackgroundColor(getResources().getColor(R.color.lightGrey));
                launchDrawer.setBackgroundColor(getResources().getColor(R.color.lightGrey));
                transaction.commit();
                navigationView.getMenu().getItem(0).setChecked(true);
                editor.putBoolean("lastBackPage",false);
                editor.commit();
            }
        }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        /*
        if (id==currentMenuItem){
            drawer.closeDrawer(GravityCompat.START);
            return false;
        }
        else
        **/
        if (id == R.id.nav_home) {
            //getSupportActionBar().setTitle("الرئيسية");
            title.setText("الرئيسية");
            Home h=new Home();
            currentFragment = h;
            FragmentTransaction transaction=getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.fragmentcontainer,h).addToBackStack(null);
            toolbar.setBackgroundColor(getResources().getColor(R.color.lightGrey));
            search.setBackgroundColor(getResources().getColor(R.color.lightGrey));
            notifications.setBackgroundColor(getResources().getColor(R.color.lightGrey));
            launchDrawer.setBackgroundColor(getResources().getColor(R.color.lightGrey));
            transaction.commit();
        } else if (id == R.id.nav_categories) {
            //getSupportActionBar().setTitle("الأصناف");
            title.setText("الأصناف");
            Categories c=new Categories();
            currentFragment = c;
            FragmentTransaction transaction=getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.fragmentcontainer,c);
            toolbar.setBackgroundColor(getResources().getColor(R.color.lightGrey));
            search.setBackgroundColor(getResources().getColor(R.color.lightGrey));
            notifications.setBackgroundColor(getResources().getColor(R.color.lightGrey));
            launchDrawer.setBackgroundColor(getResources().getColor(R.color.lightGrey));
            transaction.commit();
        } else if (id == R.id.nav_products) {
            try{
                //getSupportActionBar().setTitle("كل المنتجات");
                title.setText("كل المنتجات");
                Products p=new Products();
                currentFragment = p;
                FragmentTransaction transaction=getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.fragmentcontainer,p).addToBackStack(null);
                toolbar.setBackgroundColor(getResources().getColor(R.color.lightGrey));
                search.setBackgroundColor(getResources().getColor(R.color.lightGrey));
                notifications.setBackgroundColor(getResources().getColor(R.color.lightGrey));
                launchDrawer.setBackgroundColor(getResources().getColor(R.color.lightGrey));
                transaction.commit();
            }catch (Exception e){
                
            }

        } else if (id == R.id.nav_offers) {
            //getSupportActionBar().setTitle("العروض");
            title.setText("العروض");
            Offers o=new Offers();
            currentFragment = o;
            FragmentTransaction transaction=getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.fragmentcontainer,o).addToBackStack(null);
            toolbar.setBackgroundColor(getResources().getColor(R.color.lightGrey));
            search.setBackgroundColor(getResources().getColor(R.color.lightGrey));
            notifications.setBackgroundColor(getResources().getColor(R.color.lightGrey));
            launchDrawer.setBackgroundColor(getResources().getColor(R.color.lightGrey));
            transaction.commit();
        } else if (id == R.id.nav_we) {
            //getSupportActionBar().setTitle("من نحن");
            //change Toolbar color
            title.setText("من نحن");
            We w=new We();
            currentFragment = w;
            FragmentTransaction transaction=getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.fragmentcontainer,w).addToBackStack(null);
            toolbar.setBackgroundColor(getResources().getColor(R.color.colorWhite));
            search.setBackgroundColor(getResources().getColor(R.color.colorWhite));
            notifications.setBackgroundColor(getResources().getColor(R.color.colorWhite));
            launchDrawer.setBackgroundColor(getResources().getColor(R.color.colorWhite));
            transaction.commit();
        } else if (id == R.id.nav_about) {
            //getSupportActionBar().setTitle("حول التطبيق");
            title.setText("حول التطبيق");
            About a=new About();
            currentFragment = a;
            FragmentTransaction transaction=getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.fragmentcontainer,a).addToBackStack(null);
            toolbar.setBackgroundColor(getResources().getColor(R.color.colorWhite));
            search.setBackgroundColor(getResources().getColor(R.color.colorWhite));
            notifications.setBackgroundColor(getResources().getColor(R.color.colorWhite));
            launchDrawer.setBackgroundColor(getResources().getColor(R.color.colorWhite));
            transaction.commit();
        } else if (id == R.id.nav_conversation) {
            //getSupportActionBar().setTitle("الدردشة");
            title.setText("الدعم الفني");
            Connect c=new Connect();
            currentFragment = c;
            FragmentTransaction transaction=getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.fragmentcontainer,c).addToBackStack(null);
            toolbar.setBackgroundColor(getResources().getColor(R.color.lightGrey));
            search.setBackgroundColor(getResources().getColor(R.color.lightGrey));
            notifications.setBackgroundColor(getResources().getColor(R.color.lightGrey));
            launchDrawer.setBackgroundColor(getResources().getColor(R.color.lightGrey));
            transaction.commit();
        }else if (id == R.id.nav_settings) {
            //getSupportActionBar().setTitle("الإعدادات");
            title.setText("الإعدادات");
            Settings s=new Settings();
            currentFragment = s;
            FragmentTransaction transaction=getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.fragmentcontainer,s).addToBackStack(null);
            toolbar.setBackgroundColor(getResources().getColor(R.color.lightGrey));
            search.setBackgroundColor(getResources().getColor(R.color.lightGrey));
            notifications.setBackgroundColor(getResources().getColor(R.color.lightGrey));
            launchDrawer.setBackgroundColor(getResources().getColor(R.color.lightGrey));
            transaction.commit();
        }
        else if (id == R.id.nav_exit) {
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            finish();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
/*
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId()==R.id.search){
        //Intent intent = new Intent(MainActivity.this, SearchActivity.class);
        //startActivity(intent);
            drawer.openDrawer(Gravity.RIGHT);
        return true;
        }else if (item.getItemId()==R.id.notifications){
            Intent intent = new Intent(MainActivity.this, Notifications.class);
            startActivity(intent);
        }
        return false;
    }
    **/
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

    }
    public void firstFragmentLaunch(){
        SharedPreferences sharedPreferences = getSharedPreferences("Extra",Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        if (sharedPreferences.getString("target","").equals("search")){
            title.setText("البحث");
            Search s=new Search();
            FragmentTransaction transaction=getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.fragmentcontainer,s);
            toolbar.setBackgroundColor(getResources().getColor(R.color.lightGrey));
            search.setBackgroundColor(getResources().getColor(R.color.lightGrey));
            notifications.setBackgroundColor(getResources().getColor(R.color.lightGrey));
            launchDrawer.setBackgroundColor(getResources().getColor(R.color.lightGrey));
            transaction.commit();
            editor.putString("target","main");
            editor.commit();
        }else {
            Home h=new Home();
            currentFragment = h;
            FragmentTransaction transaction=getSupportFragmentManager().beginTransaction();
            transaction.replace(R.id.fragmentcontainer,h);
            toolbar.setBackgroundColor(getResources().getColor(R.color.lightGrey));
            search.setBackgroundColor(getResources().getColor(R.color.lightGrey));
            notifications.setBackgroundColor(getResources().getColor(R.color.lightGrey));
            launchDrawer.setBackgroundColor(getResources().getColor(R.color.lightGrey));
            transaction.commit();
        }
    }
    private void callUpdateProfileAPI(String fcm_token){
        SharedPreferences sharedPreferences = getSharedPreferences("Profile",Context.MODE_PRIVATE);
        final String token = sharedPreferences.getString("token","");
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request newRequest  = chain.request().newBuilder()
                        .addHeader("Authorization", "Bearer " + token)
                        .build();
                return chain.proceed(newRequest);
            }
        }).build();
        String mobileNumber = database.getProfile().getMobile_number();
        String productFlag = booleanConverter(database.getProfile().getProduct_notification_flag());
        String offerFlag = booleanConverter(database.getProfile().getOffer_notification_flag());
        Retrofit retrofit = new Retrofit.Builder().client(client).baseUrl(BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
        UpdateProfileAPI updateAPI = retrofit.create(UpdateProfileAPI.class);
        Call<BaseResponse> call;

        if (mobileNumber.equals("")){
            call = updateAPI.update("0000000000",productFlag,offerFlag,fcm_token);
        }else {
            call = updateAPI.update(mobileNumber,productFlag,offerFlag,fcm_token);
        }
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                String json = new Gson().toJson(response.body().getData());
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
            }
        });
    }
    public String booleanConverter(int input){
        if (input==0){
            return "no";
        }else {
            return "yes";
        }
    }
}
