package com.apps.scit.e_store.API;

import com.apps.scit.e_store.Model.BaseResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by Misheal on 7/27/2018.
 */

public interface UpdateProfileAPI {
    @FormUrlEncoded
    @POST("api/v1/update_profile")
    Call<BaseResponse> update(@Field("mobile")String mobile,
                              @Field("notifications_products")String notifications_products,
                              @Field("notifications_offers")String notifications_offers,
                              @Field("fcm_token") String fcm_token);
}
