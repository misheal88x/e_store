package com.apps.scit.e_store.API;

import com.apps.scit.e_store.Model.BaseResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by Misheal on 5/19/2019.
 */

public interface GetProductPaginatedRatesAPI {
    @GET("api/v1/product/{product_id}/ratings")
    Call<BaseResponse> getRatings(@Path("product_id") String product_id, @Query("page") int page);
}
