package com.apps.scit.e_store.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.apps.scit.e_store.Model.NotificationObject;
import com.apps.scit.e_store.R;

import java.util.List;

/**
 * Created by Misheal on 6/26/2018.
 */

public class NotificationsAdapter extends RecyclerView.Adapter<NotificationsAdapter.MyViewHolder> {
    private Context context;
    private List<NotificationObject> listOfNotifications;
    public class MyViewHolder extends RecyclerView.ViewHolder{
        public TextView text,date;
        public MyViewHolder(View view){
            super(view);
            text = view.findViewById(R.id.notification_text);
            date = view.findViewById(R.id.notification_date);
        }
    }
    public NotificationsAdapter(Context context, List<NotificationObject> listOfNotifications){
        this.context = context;
        this.listOfNotifications = listOfNotifications;
    }
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_notifications,parent,false);
        return new MyViewHolder(itemView);
    }
    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        NotificationObject notification = listOfNotifications.get(position);
        holder.text.setText(notification.getContent());
        holder.date.setText(notification.getCreated_at());
    }
    @Override
    public int getItemCount() {
        return listOfNotifications.size();
    }
}
