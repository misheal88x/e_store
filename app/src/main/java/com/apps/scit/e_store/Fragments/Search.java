package com.apps.scit.e_store.Fragments;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.apps.scit.e_store.Activities.SearchResults;
import com.apps.scit.e_store.Extra.IOnBackPressed;
import com.apps.scit.e_store.R;

/**
 * Created by Misheal on 7/20/2018.
 */

public class Search extends android.support.v4.app.Fragment implements IOnBackPressed{
    RadioGroup mainSortRadioGroup;
    RadioButton newer,value,name,rate,visit;
    CheckBox by_product,by_category,by_offer;
    String choosedSortType ;
    Boolean isProductChoosed;
    Boolean isCategoryChoosed;
    Boolean isOfferChoosed;
    EditText searchText;
    Button performSearch;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    //Sort type shared preferences
    SharedPreferences typeShared;
    SharedPreferences.Editor typeEditor;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.search_fragment,container,false);
        typeShared = getContext().getSharedPreferences("Sort",Context.MODE_PRIVATE);
        typeEditor = typeShared.edit();
        choosedSortType = typeShared.getString("SearchSort","by_name");
        isProductChoosed = true;
        isCategoryChoosed = true;
        isOfferChoosed = true;
        mainSortRadioGroup = view.findViewById(R.id.search_sort_main_group);
        newer = view.findViewById(R.id.search_sort_by_newer);
        value = view.findViewById(R.id.search_sort_by_value);
        name = view.findViewById(R.id.search_sort_by_name);
        rate = view.findViewById(R.id.search_sort_by_rate);
        visit = view.findViewById(R.id.search_sort_by_visit);
        sharedPreferences = getContext().getSharedPreferences("Extra", Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
        editor.putBoolean("searchFragmanet",false);
        editor.commit();
        setDefaultChecked(typeShared.getString("SearchSort","by_name"));
        by_product = view.findViewById(R.id.search_by_product_checkbox);
        by_category = view.findViewById(R.id.search_by_category_checkbox);
        by_offer = view.findViewById(R.id.search_by_offer_checkbox);
        by_product.setChecked(isProductChoosed);
        by_category.setChecked(isCategoryChoosed);
        by_offer.setChecked(isOfferChoosed);
        searchText = view.findViewById(R.id.search_text);
        performSearch = view.findViewById(R.id.search_perform);
        //CheckBoxes----------------------------------------------------------------------------------
        by_product.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                isProductChoosed=b;
            }
        });
        by_category.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                isCategoryChoosed = b;
            }
        });
        by_offer.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                isOfferChoosed = b;
            }
        });
        //RadioButtons--------------------------------------------------------------------------------
        newer.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b){
                    value.setChecked(false);
                    name.setChecked(false);
                    rate.setChecked(false);
                    visit.setChecked(false);
                    choosedSortType = "by_date";
                    typeEditor.putString("SearchSort","by_date");
                    typeEditor.commit();
                }
            }
        });
        value.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b){
                    newer.setChecked(false);
                    name.setChecked(false);
                    rate.setChecked(false);
                    visit.setChecked(false);
                    choosedSortType = "by_price";
                    typeEditor.putString("SearchSort","by_price");
                    typeEditor.commit();
                }
            }
        });
        name.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b){
                    value.setChecked(false);
                    newer.setChecked(false);
                    rate.setChecked(false);
                    visit.setChecked(false);
                    choosedSortType = "by_name";
                    typeEditor.putString("SearchSort","by_name");
                    typeEditor.commit();
                }
            }
        });
        rate.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b){
                    value.setChecked(false);
                    name.setChecked(false);
                    newer.setChecked(false);
                    visit.setChecked(false);
                    choosedSortType = "by_average";
                    typeEditor.putString("SearchSort","by_average");
                    typeEditor.commit();
                }
            }
        });
        visit.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b){
                    value.setChecked(false);
                    name.setChecked(false);
                    rate.setChecked(false);
                    newer.setChecked(false);
                    choosedSortType = "by_visiting";
                    typeEditor.putString("SearchSort","by_visiting");
                    typeEditor.commit();
                }
            }
        });
        performSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (searchText.getText().toString().equals("")){
                    Toast.makeText(getContext(), "لم تقم بتحديد ما تريد البحث عنه", Toast.LENGTH_SHORT).show();
                    return;
                }else {
                    Intent intent = new Intent(getContext(), SearchResults.class);
                    intent.putExtra("searchText",searchText.getText().toString());
                    intent.putExtra("products",booleanToNumConvert(isProductChoosed));
                    intent.putExtra("categories",booleanToNumConvert(isCategoryChoosed));
                    intent.putExtra("offers",booleanToNumConvert(isOfferChoosed));
                    intent.putExtra("sortBy",choosedSortType);
                    startActivity(intent);
                }

            }
        });
        return view;
    }
    public int booleanToNumConvert(Boolean input){
        if (input){
            return 1;
        }else {
            return 0;
        }
    }

    @Override
    public boolean onBackPressed() {
        editor.putBoolean("searchFragmanet",true);
        editor.commit();
        return true;
    }
public void setDefaultChecked(String type){
       switch (type){
           case "by_date":newer.setChecked(true);break;
           case "by_price":value.setChecked(true);break;
           case "by_name":name.setChecked(true);break;
           case "by_average":rate.setChecked(true);break;
           case "by_visiting":visit.setChecked(true);break;
       }
}
    /*
    @Override
    public boolean onBackPressed() {
        return true;
    }
    **/
}
