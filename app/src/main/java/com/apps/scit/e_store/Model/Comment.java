package com.apps.scit.e_store.Model;

/**
 * Created by Misheal on 6/23/2018.
 */

public class Comment {
    private String username;
    private float stars;
    private String commentText;
    public Comment(String username, float stars, String commentText) {
        this.username = username;
        this.stars = stars;
        this.commentText = commentText;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public float getStars() {
        return stars;
    }

    public void setStars(float stars) {
        this.stars = stars;
    }

    public String getCommentText() {
        return commentText;
    }

    public void setCommentText(String text) {
        this.commentText = text;
    }
}
