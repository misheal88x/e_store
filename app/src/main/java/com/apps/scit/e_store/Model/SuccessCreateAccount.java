package com.apps.scit.e_store.Model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Misheal on 7/9/2018.
 */

public class SuccessCreateAccount {
    private List<ConfigMainObject> configs = new ArrayList<>();
    private List<SliderObject> slider = new ArrayList<>();
    private List<Category> categories = new ArrayList<>();
    private List<Product> products = new ArrayList<>();
    private List<Product> normal_offers = new ArrayList<>();
    private List<Offer> special_offers = new ArrayList<>();
    private List<Product> top_searched_products = new ArrayList<>();
    private List<Product> top_rated_products = new ArrayList<>();
    private List<Product> top_viewed_products = new ArrayList<>();
    private ProfileObject profile = new ProfileObject();

    public List<ConfigMainObject> getConfigs() {
        return configs;
    }

    public void setConfigs(List<ConfigMainObject> configs) {
        this.configs = configs;
    }

    public List<SliderObject> getSlider() {
        return slider;
    }

    public void setSlider(List<SliderObject> slider) {
        this.slider = slider;
    }

    public List<Category> getCategories() {
        return categories;
    }

    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }

    public List<Product> getNormal_offers() {
        return normal_offers;
    }

    public void setNormal_offers(List<Product> normal_offers) {
        this.normal_offers = normal_offers;
    }

    public List<Offer> getSpecial_offers() {
        return special_offers;
    }

    public void setSpecial_offers(List<Offer> special_offers) {
        this.special_offers = special_offers;
    }

    public List<Product> getTop_searched_products() {
        return top_searched_products;
    }

    public void setTop_searched_products(List<Product> top_searched_products) {
        this.top_searched_products = top_searched_products;
    }

    public List<Product> getTop_rated_products() {
        return top_rated_products;
    }

    public void setTop_rated_products(List<Product> top_rated_products) {
        this.top_rated_products = top_rated_products;
    }

    public List<Product> getTop_viewed_products() {
        return top_viewed_products;
    }

    public void setTop_viewed_products(List<Product> top_viewed_products) {
        this.top_viewed_products = top_viewed_products;
    }

    public ProfileObject getProfile() {
        return profile;
    }

    public void setProfile(ProfileObject profile) {
        this.profile = profile;
    }
}
