package com.apps.scit.e_store.Model;

/**
 * Created by Misheal on 6/26/2018.
 */

public class Notification {
    private String notificationText;
    private String notificationDate;

    public Notification(String notificationText, String notificationDate) {
        this.notificationText = notificationText;
        this.notificationDate = notificationDate;
    }

    public String getNotificationText() {
        return notificationText;
    }

    public void setNotificationText(String notificationText) {
        this.notificationText = notificationText;
    }

    public String getNotificationDate() {
        return notificationDate;
    }

    public void setNotificationDate(String notificationDate) {
        this.notificationDate = notificationDate;
    }
}
