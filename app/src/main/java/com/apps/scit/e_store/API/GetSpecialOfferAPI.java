package com.apps.scit.e_store.API;

import com.apps.scit.e_store.Model.BaseResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Created by Misheal on 6/23/2019.
 */

public interface GetSpecialOfferAPI {
    @GET("api/v1/offers/special/{offer_id}")
    Call<BaseResponse> getInfo(@Path("offer_id") String offer_id);
}
