package com.apps.scit.e_store.Model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Misheal on 7/30/2018.
 */

public class SearchResultObject {
    private List<Category> categories = new ArrayList<>();
    private ProductsBaseObject products = null;
    private ProductsBaseObject normal_offers = null;
    private List<Offer> special_offers = new ArrayList<>();

    public List<Category> getCategories() {
        return categories;
    }

    public void setCategories(List<Category> categories) {
        this.categories = categories;
    }

    public ProductsBaseObject getProducts() {
        return products;
    }

    public void setProducts(ProductsBaseObject products) {
        this.products = products;
    }

    public ProductsBaseObject getNormal_offers() {
        return normal_offers;
    }

    public void setNormal_offers(ProductsBaseObject normal_offers) {
        this.normal_offers = normal_offers;
    }

    public List<Offer> getSpecial_offers() {
        return special_offers;
    }

    public void setSpecial_offers(List<Offer> special_offers) {
        this.special_offers = special_offers;
    }
}
