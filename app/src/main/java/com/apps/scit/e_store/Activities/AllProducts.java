package com.apps.scit.e_store.Activities;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.support.annotation.ColorInt;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AnimationUtils;
import android.view.animation.LayoutAnimationController;
import android.widget.ArrayAdapter;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.apps.scit.e_store.API.GetAllProductsAPI;
import com.apps.scit.e_store.API.GetSortedProductsAPI;
import com.apps.scit.e_store.Adapters.OurProductsAdapter;
import com.apps.scit.e_store.Extra.EndlessRecyclerViewScrollListener;
import com.apps.scit.e_store.Model.BaseResponse;
import com.apps.scit.e_store.Model.Product;
import com.apps.scit.e_store.Model.ProductsBaseObject;
import com.apps.scit.e_store.R;
import com.google.gson.Gson;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class AllProducts extends AppCompatActivity {

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
    Toolbar toolbar;
    TextView toolbarTitle;
    FloatingActionButton sortFab;
    RecyclerView productsRecycler;
    List<Product> listOfProducts;
    OurProductsAdapter adapter;
    ProgressBar loadData,loadMore;
    String[] choices;
    int choosenItem = 0;
    private int pageNumber = 1;
    private int sortedPageNumber = 1;
    private int per_page = 20;
    private int itemCount = 10;
    private Boolean isLoading = true;
    private int pastVisibleItems,visibleItemCount,totalItemCount,previousTotal = 0;
    private int viewThreshold = 10;
    Boolean isSorted;
    String sortingType;
    private static String BASE_URL;
    SharedPreferences typeShared;
    SharedPreferences.Editor editor;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_products);
        definingIDs();
        setSupportActionBar(toolbar);
        if (getSupportActionBar()!=null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        getSupportActionBar().setTitle("");
        toolbarTitle.setText("المنتجات");
        typeShared = getSharedPreferences("Sort",Context.MODE_PRIVATE);
        editor = typeShared.edit();
        isSorted = false;
        sortingType = typeShared.getString("AllProductsSort","by_name");
        hideActivityElements();
        listOfProducts = new ArrayList<>();
        getProducts(pageNumber);
        adapter = new OurProductsAdapter(this,listOfProducts);
        final GridLayoutManager layoutManager2 = new GridLayoutManager(AllProducts.this,2);
        productsRecycler.setLayoutManager(layoutManager2);
        runAnimation(productsRecycler,0,adapter);
        //productsRecycler.setAdapter(adapter);
        productsRecycler.addOnScrollListener(new EndlessRecyclerViewScrollListener(layoutManager2) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                if (isSorted){
                    if (listOfProducts.size()>=per_page){
                        sortedPageNumber++;
                        performSortedPagination(sortedPageNumber,sortingType);
                    }
                }else {
                    if (listOfProducts.size()>=per_page){
                        pageNumber++;
                        performPagination(pageNumber);
                    }
                }
            }
        });
        choices = new String[]{"الأحدث","الأعلى سعرا","الاسم","التقييم","الأكثر زيارة"};

        sortFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final AlertDialog.Builder builder = new AlertDialog.Builder(AllProducts.this);
                builder.setTitle("قم باختيار طريقة الترتيب").setSingleChoiceItems(new ArrayAdapter<String>(AllProducts.this, R.layout.rtl_list_item, R.id.text, choices),
                        convertSortTextToNum(typeShared.getString("AllProductsSort","by_name")), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        choosenItem = i;
                    }
                }).
                        setPositiveButton("حسنا", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                choosenItem = ((AlertDialog)dialogInterface).getListView().getCheckedItemPosition();;
                                switch (choosenItem){
                                    case 0:{
                                        isSorted = true;
                                        sortingType = "by_date";
                                        editor.putString("AllProductsSort","by_date");
                                        editor.commit();
                                        sortedPageNumber = 1;
                                        listOfProducts.clear();
                                        getSortedProducts(1,sortingType);
                                    break;
                                    }
                                    case 1: {
                                        isSorted = true;
                                        sortingType = "by_price";
                                        editor.putString("AllProductsSort","by_price");
                                        editor.commit();
                                        sortedPageNumber = 1;
                                        listOfProducts.clear();
                                        getSortedProducts(1,sortingType);
                                        break;
                                    }
                                    case 2:{
                                        isSorted = true;
                                        sortingType = "by_name";
                                        editor.putString("AllProductsSort","by_name");
                                        editor.commit();
                                        sortedPageNumber = 1;
                                        listOfProducts.clear();
                                        getSortedProducts(1,sortingType);
                                        break;
                                    }
                                    case 3:{
                                        isSorted = true;
                                        sortingType = "by_average";
                                        editor.putString("AllProductsSort","by_average");
                                        editor.commit();
                                        sortedPageNumber = 1;
                                        listOfProducts.clear();
                                        getSortedProducts(1,sortingType);
                                        break;
                                    }
                                    case 4:{
                                        isSorted = true;
                                        sortingType = "by_visiting";
                                        editor.putString("AllProductsSort","by_visiting");
                                        editor.commit();
                                        sortedPageNumber = 1;
                                        listOfProducts.clear();
                                        getSortedProducts(1,sortingType);
                                        break;
                                    }
                                }
                            }
                        }).show();
            }
        });


    }
    public class LineItemDecoration extends android.support.v7.widget.DividerItemDecoration {

        public LineItemDecoration(Context context, int orientation, @ColorInt int color) {
            super(context, orientation);

            setDrawable(new ColorDrawable(color));
        }

        public LineItemDecoration(Context context, int orientation, @NonNull Drawable drawable) {
            super(context, orientation);

            setDrawable(drawable);
        }
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.all_products_menu,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId()==android.R.id.home){
            this.finish();
        }
        return true;
    }
    public void definingIDs(){
        //Base url
        BASE_URL = getResources().getString(R.string.base_url);
        //Toolbar
        toolbar = findViewById(R.id.all_products_toolbar);
        toolbarTitle = toolbar.findViewById(R.id.basicToolbarTitle);
        //Floating Action Button
        sortFab = findViewById(R.id.all_products_sort_products_fab1);
        //RecyclerView
        productsRecycler = findViewById(R.id.all_products_recycler1);
        //ProgressBars
        loadData = findViewById(R.id.all_products_load_data);
        loadMore = findViewById(R.id.all_products_load_more);
    }
    public void showActivityElements(){
        sortFab.setVisibility(View.VISIBLE);
        productsRecycler.setVisibility(View.VISIBLE);
        loadData.setVisibility(View.GONE);
    }
    public void hideActivityElements(){
        sortFab.setVisibility(View.GONE);
        productsRecycler.setVisibility(View.GONE);
        loadData.setVisibility(View.VISIBLE);
    }
    public void getProducts(int page){
        SharedPreferences sharedPreferences = getSharedPreferences("Profile",Context.MODE_PRIVATE);
        final String token = sharedPreferences.getString("token","");
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request newRequest  = chain.request().newBuilder()
                        .addHeader("Authorization", "Bearer " + token)
                        .build();
                return chain.proceed(newRequest);
            }
        }).build();
        Retrofit retrofit = new Retrofit.Builder().client(client).baseUrl(BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
        GetAllProductsAPI getAllProductsAPI = retrofit.create(GetAllProductsAPI.class);
        Call<BaseResponse> call;
        call = getAllProductsAPI.getProducts(page);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                if (response.body().getCode()==200&&response.body().getMessage_code()==108){
                    String json = new Gson().toJson(response.body().getData());
                    ProductsBaseObject success = new Gson().fromJson(json,ProductsBaseObject.class);
                    per_page = success.getPer_page();
                    if (success.getData().size()>0){
                        for (Product p : success.getData()){
                            listOfProducts.add(p);
                            adapter.notifyDataSetChanged();
                        }
                        showActivityElements();
                    }else {
                        Toast.makeText(AllProducts.this, "لا يوجد منتجات", Toast.LENGTH_SHORT).show();
                        finish();
                    }
                }else {
                    Toast.makeText(AllProducts.this, "حدث خطأ ما!!", Toast.LENGTH_SHORT).show();
                    finish();
                }
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                Toast.makeText(AllProducts.this, "لا يوجد اتصال بالانترنت!", Toast.LENGTH_SHORT).show();
                finish();
            }
        });
/*
        RestAdapter getProducts = new RestAdapter.Builder().setClient().setEndpoint(BASE_URL).build();
        GetAllProductsAPI api = getProducts.create(GetAllProductsAPI.class);
        api.getProducts(page, new Callback<BaseResponse>() {
            @Override
            public void success(BaseResponse baseResponse, Response response) {
                if (baseResponse.getCode()==200&&baseResponse.getMessage_code()==108){
                    String json = new Gson().toJson(baseResponse.getData());
                    ProductsBaseObject success = new Gson().fromJson(json,ProductsBaseObject.class);
                    if (success.getData().size()>0){
                        for (Product p : success.getData()){
                            listOfProducts.add(p);
                            adapter.notifyDataSetChanged();
                        }
                        showActivityElements();
                    }else {
                        Toast.makeText(AllProducts.this, "لا يوجد منتجات", Toast.LENGTH_SHORT).show();
                        finish();
                    }
                }else {
                    Toast.makeText(AllProducts.this, "حدث خطأ ما!!", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                Toast.makeText(AllProducts.this, "حدث خطأ ما!", Toast.LENGTH_SHORT).show();
            }
        });
        **/
    }

    public void getSortedProducts(int page,String type){
        loadData.setVisibility(View.VISIBLE);
        SharedPreferences sharedPreferences = getSharedPreferences("Profile",Context.MODE_PRIVATE);
        final String token = sharedPreferences.getString("token","");
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request newRequest  = chain.request().newBuilder()
                        .addHeader("Authorization", "Bearer " + token)
                        .build();
                return chain.proceed(newRequest);
            }
        }).build();
        Retrofit retrofit = new Retrofit.Builder().client(client).baseUrl(BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
        GetSortedProductsAPI api = retrofit.create(GetSortedProductsAPI.class);
        Call<BaseResponse> call;
        call = api.getProducts(type,page);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                if (response.body().getCode()==200&&response.body().getMessage_code()==108){
                    String json = new Gson().toJson(response.body().getData());
                    ProductsBaseObject success = new Gson().fromJson(json,ProductsBaseObject.class);
                    per_page = success.getPer_page();
                    if (success.getData().size()>0){
                        for (Product p : success.getData()){
                            listOfProducts.add(p);
                            adapter.notifyDataSetChanged();
                        }
                        loadData.setVisibility(View.GONE);
                    }else {
                        Toast.makeText(AllProducts.this, "لا يوجد منتجات", Toast.LENGTH_SHORT).show();
                        finish();
                    }
                }else {
                    Toast.makeText(AllProducts.this, "حدث خطأ ما!!", Toast.LENGTH_SHORT).show();
                    finish();
                }
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                Toast.makeText(AllProducts.this, "لا يوجد اتصال بالانترنت!", Toast.LENGTH_SHORT).show();
                finish();
            }
        });
    }
    private void performPagination(int page){
        loadMore.setVisibility(View.VISIBLE);
        SharedPreferences sharedPreferences = getSharedPreferences("Profile",Context.MODE_PRIVATE);
        final String token = sharedPreferences.getString("token","");
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request newRequest  = chain.request().newBuilder()
                        .addHeader("Authorization", "Bearer " + token)
                        .build();
                return chain.proceed(newRequest);
            }
        }).build();
        Retrofit retrofit = new Retrofit.Builder().client(client).baseUrl(BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
        GetAllProductsAPI getAllProductsAPI = retrofit.create(GetAllProductsAPI.class);
        Call<BaseResponse> call;
        call = getAllProductsAPI.getProducts(page);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                if (response.body().getCode()==200&&response.body().getMessage_code()==108){
                    String json = new Gson().toJson(response.body().getData());
                    ProductsBaseObject success = new Gson().fromJson(json,ProductsBaseObject.class);
                    if (success.getData().size()>0){
                        for (Product p : success.getData()){
                            listOfProducts.add(p);
                            adapter.notifyDataSetChanged();
                        }

                    }else {
                        Toast.makeText(AllProducts.this, "لا يوجد منتجات إضافية", Toast.LENGTH_SHORT).show();
                    }
                }else {
                    Toast.makeText(AllProducts.this, "حدث خطأ ما!!", Toast.LENGTH_SHORT).show();
                }
                loadMore.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                Toast.makeText(AllProducts.this, "لا يوجد اتصال بالانترنت!", Toast.LENGTH_SHORT).show();
                loadMore.setVisibility(View.GONE);
            }
        });
    }
    public void performSortedPagination(int page,String type){
        loadMore.setVisibility(View.VISIBLE);
        SharedPreferences sharedPreferences = getSharedPreferences("Profile",Context.MODE_PRIVATE);
        final String token = sharedPreferences.getString("token","");
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request newRequest  = chain.request().newBuilder()
                        .addHeader("Authorization", "Bearer " + token)
                        .build();
                return chain.proceed(newRequest);
            }
        }).build();
        Retrofit retrofit = new Retrofit.Builder().client(client).baseUrl(BASE_URL).addConverterFactory(GsonConverterFactory.create()).build();
        GetSortedProductsAPI api = retrofit.create(GetSortedProductsAPI.class);
        Call<BaseResponse> call;
        call = api.getProducts(type,page);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                if (response.body().getCode()==200&&response.body().getMessage_code()==108){
                    String json = new Gson().toJson(response.body().getData());
                    ProductsBaseObject success = new Gson().fromJson(json,ProductsBaseObject.class);
                    if (success.getData().size()>0){
                        for (Product p : success.getData()){
                            listOfProducts.add(p);
                            adapter.notifyDataSetChanged();
                        }
                     loadMore.setVisibility(View.GONE);
                    }else {
                        Toast.makeText(AllProducts.this, "لا يوجد منتجات إضافية", Toast.LENGTH_SHORT).show();
                    }
                }else {
                    Toast.makeText(AllProducts.this, "حدث خطأ ما!!", Toast.LENGTH_SHORT).show();
                }
                loadMore.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<BaseResponse> call, Throwable t) {
                Toast.makeText(AllProducts.this, "لا يوجد اتصال بالانترنت!", Toast.LENGTH_SHORT).show();
                loadMore.setVisibility(View.GONE);
            }
        });
    }
    public int convertSortTextToNum(String type){
        int output = 0;
        switch (type){
            case "by_date":output=0;break;
            case "by_price":output=1;break;
            case "by_name":output=2;break;
            case "by_average":output=3;break;
            case "by_visiting":output=4;break;
        }
        return output;
    }
    public void runAnimation(RecyclerView recyclerView,int type,OurProductsAdapter adapter){
        Context context = recyclerView.getContext();
        LayoutAnimationController controller = null;
        if (type == 0){ // Fall down animation
            controller = AnimationUtils.loadLayoutAnimation(context,R.anim.layout_fall_down_vertical);
            recyclerView.setAdapter(adapter);
            recyclerView.setLayoutAnimation(controller);
            recyclerView.getAdapter().notifyDataSetChanged();
            recyclerView.scheduleLayoutAnimation();
        }
    }
}
