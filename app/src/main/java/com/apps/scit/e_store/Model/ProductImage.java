package com.apps.scit.e_store.Model;

/**
 * Created by Misheal on 7/29/2018.
 */

public class ProductImage {
    private int id = 0;
    private String image = "";
    private int product_id = 0;

    public ProductImage(int id, String image, int product_id) {
        this.id = id;
        this.image = image;
        this.product_id = product_id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public int getProduct_id() {
        return product_id;
    }

    public void setProduct_id(int product_id) {
        this.product_id = product_id;
    }
}
