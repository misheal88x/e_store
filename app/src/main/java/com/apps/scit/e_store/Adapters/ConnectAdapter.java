package com.apps.scit.e_store.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.apps.scit.e_store.Model.Message;
import com.apps.scit.e_store.R;

import java.util.List;

/**
 * Created by Misheal on 6/21/2018.
 */

public class ConnectAdapter extends RecyclerView.Adapter<ConnectAdapter.MyViewHolder>{
private Context context;
private List<Message> listOfMessages;
public class MyViewHolder extends RecyclerView.ViewHolder{
    //Admin message
    public RelativeLayout adminLayout;
    public ImageView adminImage;
    public TextView adminName,adminDate,adminText;
    //User message
    public RelativeLayout userLayout;
    public ImageView userImage;
    public TextView userName,userDate,userText;
    public MyViewHolder(View view){
        super(view);
        //Admin message
        adminLayout = view.findViewById(R.id.admin_message_layout);
        adminImage = view.findViewById(R.id.admin_message_image);
        adminName = view.findViewById(R.id.admin_message_username);
        adminDate = view.findViewById(R.id.admin_message_date);
        adminText = view.findViewById(R.id.admin_message_text);
        //User message
        userLayout = view.findViewById(R.id.user_message_layout);
        userImage = view.findViewById(R.id.user_message_image);
        userName = view.findViewById(R.id.user_message_username);
        userDate = view.findViewById(R.id.user_message_date);
        userText = view.findViewById(R.id.user_message_text);
    }
}
    public ConnectAdapter(Context context, List<Message> listOfMessages){
        this.context = context;
        this.listOfMessages = listOfMessages;
    }
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.message_row,parent,false);
        return new MyViewHolder(itemView);
    }
    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Message message = listOfMessages.get(position);
        if (message.getReceiver_id()!=0){
            holder.adminLayout.setVisibility(View.VISIBLE);
            holder.userLayout.setVisibility(View.GONE);
            holder.adminName.setText("الإدارة");
            holder.adminDate.setText(message.getMsg_time());
            holder.adminText.setText(message.getContent());
        }
        else {
            holder.userLayout.setVisibility(View.VISIBLE);
            holder.adminLayout.setVisibility(View.GONE);
            holder.userName.setText("user "+String.valueOf(message.getSender_id()));
            holder.userDate.setText(message.getMsg_time());
            holder.userText.setText(message.getContent());
        }
    }
    @Override
    public int getItemCount() {
        return listOfMessages.size();
    }
}
