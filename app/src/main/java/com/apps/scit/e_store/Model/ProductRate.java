package com.apps.scit.e_store.Model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Misheal on 7/29/2018.
 */

public class ProductRate {
    @SerializedName("id") private int id = 0;
    @SerializedName("value") private int value = 0;
    @SerializedName("comment") private String comment = "";
    @SerializedName("visitor_id") private int visitor_id = 0;
    @SerializedName("rate_time") private String rate_time = "";
    //private int product_id = 0;


    public ProductRate(int id, int value, String comment, int visitor_id, String rate_time, int product_id) {
        this.id = id;
        this.value = value;
        this.comment = comment;
        this.visitor_id = visitor_id;
        this.rate_time = rate_time;
        //this.product_id = product_id;
    }

    public ProductRate(int value, String comment, int visitor_id) {
        this.value = value;
        this.comment = comment;
        this.visitor_id = visitor_id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public int getVisitor_id() {
        return visitor_id;
    }

    public void setVisitor_id(int visitor_id) {
        this.visitor_id = visitor_id;
    }

    public String getRate_time() {
        return rate_time;
    }

    public void setRate_time(String rate_time) {
        this.rate_time = rate_time;
    }

    /*
    public int getProduct_id() {
        return product_id;
    }

    public void setProduct_id(int product_id) {
        this.product_id = product_id;
    }

    **/
}
