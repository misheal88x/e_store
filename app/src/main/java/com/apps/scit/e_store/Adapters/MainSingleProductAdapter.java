package com.apps.scit.e_store.Adapters;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.apps.scit.e_store.Activities.ProductDetails;
import com.apps.scit.e_store.Model.Product;
import com.apps.scit.e_store.R;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.glide.slider.library.svg.GlideApp;

import java.util.List;

/**
 * Created by Misheal on 7/5/2018.
 */

public class MainSingleProductAdapter  extends RecyclerView.Adapter<MainSingleProductAdapter.MyViewHolder> {
    private Context context;
    private List<Product> listOfOffers;
    public class MyViewHolder extends RecyclerView.ViewHolder{
        public TextView offerName,offerOldPrice,offerNewPrice,offerRemain;
        public ImageView offerImage;
        public RelativeLayout layout;
        public MyViewHolder(View view){
            super(view);
            offerName = view.findViewById(R.id.main_offer_card_name);
            offerOldPrice = view.findViewById(R.id.main_offer_card_old_price);
            offerNewPrice = view.findViewById(R.id.main_offer_card_new_price);
            offerImage = view.findViewById(R.id.main_offer_card_image);
            offerRemain = view.findViewById(R.id.normal_offer_remaining_time);
            offerRemain.bringToFront();
            layout = view.findViewById(R.id.main_offer_card_layout);
        }
    }
    public MainSingleProductAdapter(Context context, List<Product> listOfOffers){
        this.context = context;
        this.listOfOffers = listOfOffers;
    }
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.main_offer_card,parent,false);
        return new MyViewHolder(itemView);
    }
    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        final Product offer = listOfOffers.get(position);
        holder.offerName.setText(offer.getName());
        holder.offerOldPrice.setText(String.valueOf(Math.round(offer.getPrice()))+" ل.س");
        holder.offerNewPrice.setText(String.valueOf(Math.round(offer.getOffer_price()))+" ل.س");
        if (offer.getImage()!=null){
            GlideApp.with(context).load(offer.getImage())
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .placeholder(R.drawable.offline).centerCrop().into(holder.offerImage);
            //Picasso.with(context).load(offer.getImage()).placeholder(R.drawable.offline).into(holder.offerImage);
        }
        if (offer.getOffer_remain()==0){
            holder.offerRemain.setText("00:00:00");
        }else {
            try {
                holder.offerRemain.setText(formatSeconds(Long.parseLong(String.valueOf(offer.getOffer_remain()))));
                //holder.offerRemain.setText(formatSeconds(Long.parseLong(offer.getOffer_remain())));
            }catch (Exception e){}
            }
        holder.layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences sharedPreferences = context.getSharedPreferences("product",Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPreferences.edit();
                editor.putString("productType","offer");
                editor.putInt("product_id",offer.getId());
                editor.commit();
                Intent intent = new Intent(context,ProductDetails.class);
                intent.putExtra("product_id",String.valueOf(offer.getId()));
                intent.putExtra("productType","offer");
                context.startActivity(intent);
            }
        });
    }
    @Override
    public int getItemCount() {
        return listOfOffers.size();
    }
    public  String formatSeconds(long totalSecs){
        long days = (int)(totalSecs/86400);
        long hours =  (int)((totalSecs % 86400) / 3600);
        long minutes =  (int)((totalSecs % 3600) / 60);
        long seconds =  (int)(totalSecs % 60);
        if (days==0){
            return String.format("%02d:%02d:%02d", hours, minutes, seconds);
        }else {
            return String.format("%02d:%02d:%02d", hours, minutes, seconds)+" و "+String.valueOf(days)+" يوم";
        }

    }
}
